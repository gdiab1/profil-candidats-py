# Introduction
L’objectif de ce notebook est de déterminer le métier d’un candidat à partir des informations sur ses compétences.
A partir d'un dataset de compétences, on réalisera :
* un preprocessing des données
* Una analyse exploratoire
* Un clustering non supervisé afin d'identifier les groupes de profils techniques distincts
* Une prédiction des profils dont le métier n'est pas labellisé

Par Georges DIAB - Data & AI Engineer
